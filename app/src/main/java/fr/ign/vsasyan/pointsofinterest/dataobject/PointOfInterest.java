package fr.ign.vsasyan.pointsofinterest.dataobject;

public class PointOfInterest extends DataBaseObject {
    private String title;
    private String description;
    private Double lat;
    private Double lng;
    private Long userId;

    // Constructor
    public PointOfInterest(String title, String description, Double lat, Double lng, Long userId) {
        setTitle(title);
        setDescription(description);
        setLat(lat);
        setLng(lng);
        setUserId(userId);
    }

    public PointOfInterest(Long id, String title, String description, Double lat, Double lng, Long userId) {
        super(id);
        setTitle(title);
        setDescription(description);
        setLat(lat);
        setLng(lng);
        setUserId(userId);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}