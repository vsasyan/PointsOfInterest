package fr.ign.vsasyan.pointsofinterest.dataaccess;

import android.database.sqlite.SQLiteDatabase;

public abstract class DAO<T> {
    protected SQLiteDatabase db;

    public DAO(SQLiteDatabase db) {
        this.db = db;
    }

    /**
     * Save given obj in database
     * @param obj
     * @return
     */
    public abstract T create(T obj);

    /**
     * Update given obj in database
     * @param obj
     * @return
     */
    public abstract boolean update(T obj);

    /**
     * Delete given obj of database
     * @param obj
     * @return
     */
    public abstract boolean delete(T obj);

}
