package fr.ign.vsasyan.pointsofinterest.dataobject;

import java.io.Serializable;

public abstract class DataBaseObject implements Serializable {
    protected Long id;

    // Default constructor
    DataBaseObject() {}

    // Constructor with id
    DataBaseObject(Long id) {
        setId(id);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}