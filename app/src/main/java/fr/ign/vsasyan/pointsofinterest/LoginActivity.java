package fr.ign.vsasyan.pointsofinterest;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import fr.ign.vsasyan.pointsofinterest.dataaccess.PointOfInterestDAO;
import fr.ign.vsasyan.pointsofinterest.dataaccess.UserDAO;
import fr.ign.vsasyan.pointsofinterest.dataobject.User;

public class LoginActivity extends AppCompatActivity {

    /**
     * Déclaration des objets Java représentant les composants graphiques
     * (en attributs de la classe, ils sont donc accessibles depuis toutes les méthodes)
     */
    EditText et_email;
    EditText et_password;
    Button b_login;
    Button b_sign_in;

    // Attributs de classe concernant la gestion de base de données
    SQLiteDatabase db;
    UserDAO userDAO;
    PointOfInterestDAO pointOfInterestDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // I - Instanciation les objets Java représentant les composants graphiques
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        b_login = findViewById(R.id.b_login);
        b_sign_in = findViewById(R.id.b_sign_in);

        // II - Ajout des écouteurs d'événements aux composants graphiques représentés par des objets Java
        b_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginActivity.this.tryLogin();
            }
        });
        b_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginActivity.this.openSignInActivity();
            }
        });

        // III - Autre traitement à effectuer au début...

        // Instantiation des attributs concernants la gestion de la base de données
        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
        db = dataBaseHelper.getWritableDatabase();
        userDAO = new UserDAO(db);
        pointOfInterestDAO = new PointOfInterestDAO(db);
    }

    private void openSignInActivity() {
        String email = et_email.getText().toString();

        // Create intent
        Intent intent = new Intent(this, SignInActivity.class);

        // Put extra
        intent.putExtra(Constants.EXTRA_EMAIL, email);

        // Start activity
        startActivity(intent);
    }

    private void tryLogin() {
        String email = et_email.getText().toString();
        String password = et_password.getText().toString();
        String encoded_password = password; // On ne hash pas le password...

        User user = userDAO.findByEmailAndEncodedPassword(email, encoded_password);

        if (user != null) {
            openMapsActivity(user);
        } else {
            Toast.makeText(this, "Email or password not correct.", Toast.LENGTH_SHORT).show();
        }
    }

    private void openMapsActivity(User user) {
        // Create intent
        Intent intent = new Intent(this, MapsActivity.class);

        // Put extra
        intent.putExtra(Constants.EXTRA_USER, user);

        // Start activity
        startActivity(intent);
    }

}
