package fr.ign.vsasyan.pointsofinterest;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import fr.ign.vsasyan.pointsofinterest.dataaccess.PointOfInterestDAO;
import fr.ign.vsasyan.pointsofinterest.dataaccess.UserDAO;
import fr.ign.vsasyan.pointsofinterest.dataobject.PointOfInterest;
import fr.ign.vsasyan.pointsofinterest.dataobject.User;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    // Attributs propres à la classe
    private GoogleMap mMap;
    private User user;
    private ArrayList<Marker> markers = new ArrayList<>();
    private LatLng currentLatLng = new LatLng(42, 5); // Nul normalement mais comme ça c'est plus simple
    private Marker currentMarker;

    // Attributs de classe concernant la gestion de base de données
    SQLiteDatabase db;
    UserDAO userDAO;
    PointOfInterestDAO pointOfInterestDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Récupération de l'utilisateur passé en paramètre de l'Intent
        Intent intent = getIntent();
        if (intent != null) {
            user = (User)intent.getSerializableExtra(Constants.EXTRA_USER);
        } else {
            Toast.makeText(this, "Error no user!", Toast.LENGTH_SHORT).show();
            this.finish();
        }

        // Instantiation des attributs concernants la gestion de la base de données
        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
        db = dataBaseHelper.getWritableDatabase();
        userDAO = new UserDAO(db);
        pointOfInterestDAO = new PointOfInterestDAO(db);

        // Instanciation du LocationClient
        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationRequest locationRequest = new LocationRequest();
            locationRequest.setInterval(10000);
            locationRequest.setFastestInterval(5000);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            // LocationCallback anonyme :
            mFusedLocationClient.requestLocationUpdates(locationRequest, new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    for (Location location : locationResult.getLocations()) {
                        MapsActivity.this.updateLastLocation(location);
                    }
                }
            }, null);
        } else {
            Toast.makeText(this, "Error : impossible to get location", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected  void onResume() {
        super.onResume();
        // Mise à jour de la liste de point
        showPoints();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.maps_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_item_add_point) {
            openAddPointActivity();
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        showPoints();
    }

    protected void showPoints() {
        ArrayList<PointOfInterest> points = pointOfInterestDAO.findByUserId(user.getId());

        if (mMap != null && points != null) {
            // Remove old markers
            for (Marker marker : markers) {
                marker.remove();
            }
            // Add new from points
            for (PointOfInterest point: points) {
                LatLng latLng = new LatLng(point.getLat(), point.getLng());
                Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).title(point.getTitle()));
                markers.add(marker);
            }
            // Manage camera
            if (points.size() == 1) {
                LatLng latLng = new LatLng(points.get(0).getLat(), points.get(0).getLng());
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.moveCamera(CameraUpdateFactory.zoomTo(14));
            } else if (points.size() >= 2) {
                LatLng latLng1 = new LatLng(points.get(0).getLat(), points.get(0).getLng());
                LatLng latLng2 = new LatLng(points.get(1).getLat(), points.get(1).getLng());
                LatLngBounds  bounds = new LatLngBounds(latLng1, latLng2);
                for (PointOfInterest point: points) {
                    LatLng latLng = new LatLng(point.getLat(), point.getLng());
                    bounds = bounds.including(latLng);
                }
                mMap.moveCamera(CameraUpdateFactory.newLatLng(bounds.getCenter()));
                mMap.moveCamera(CameraUpdateFactory.zoomTo(4));
            }
        }
    }

    protected void openAddPointActivity() {
        if (currentLatLng != null) {
            // Create intent
            Intent intent = new Intent(this, NewPointActivity.class);

            // Put extra
            intent.putExtra(Constants.EXTRA_USER, user);
            intent.putExtra(Constants.EXTRA_LAT, currentLatLng.latitude);
            intent.putExtra(Constants.EXTRA_LNG, currentLatLng.longitude);

            // Start activity
            startActivity(intent);
        } else {
            Toast.makeText(this, "No Last Position", Toast.LENGTH_LONG).show();
        }
    }

    protected void updateLastLocation(Location location) {
        currentLatLng = new LatLng(location.getLatitude(), location.getLongitude());
        if (currentMarker == null) {
            currentMarker = mMap.addMarker(new MarkerOptions().position(currentLatLng).title("Current Position"));
        } else {
            currentMarker.setPosition(currentLatLng);
        }
    }
}
