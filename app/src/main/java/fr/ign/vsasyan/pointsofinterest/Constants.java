package fr.ign.vsasyan.pointsofinterest;

public class Constants {
    public static final String EXTRA_USER = "EXTRA_USER";
    public static final String EXTRA_EMAIL = "EXTRA_EMAIL";
    public static final String EXTRA_LAT = "EXTRA_LAT";
    public static final String EXTRA_LNG = "EXTRA_LNG";
}
