package fr.ign.vsasyan.pointsofinterest.dataaccess;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import fr.ign.vsasyan.pointsofinterest.dataobject.User;

public class UserDAO extends DAO<User> {

    public UserDAO(SQLiteDatabase db) {
        super(db);
    }

    @Override
    public User create(User obj) {
        ContentValues values = new ContentValues();
        values.put("email", obj.getEmail());
        values.put("encoded_password", obj.getEncodedPassword());
        values.put("first_name", obj.getFirstName());
        values.put("last_name", obj.getLastName());

        Long id = db.insert("user", null, values);

        obj.setId(id);

        return obj;
    }

    @Override
    public boolean update(User obj) {
        ContentValues values = new ContentValues();
        values.put("email", obj.getEmail());
        values.put("encoded_password", obj.getEncodedPassword());
        values.put("first_name", obj.getFirstName());
        values.put("last_name", obj.getLastName());

        // Which row to update, based on the title
        String selection = " id LIKE ?";
        String[] selectionArgs = { obj.getId().toString() };

        int count = db.update("user", values, selection, selectionArgs);

        return count == 1;
    }

    @Override
    public boolean delete(User obj) {
        // Define 'where' part of query.
        String selection = "id = ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = { obj.getId().toString() };
        // Issue SQL statement.
        int count = db.delete("user", selection, selectionArgs);

        return count == 1;
    }

    public User findByEmailAndEncodedPassword(String email, String encodesPassword) {
        String[] columns = { "id", "email", "encoded_password", "first_name", "last_name" };

        String selection = "email = ? AND encoded_password = ?";
        String[] selectionArgs = { email, encodesPassword };

        Cursor cursor = db.query(
                "user",                     // The table to query
                columns,                    // The columns to return
                selection,                  // The columns for the WHERE clause
                selectionArgs,              // The values for the WHERE clause
                null,                       // don't group the rows
                null,                       // don't filter by row groups
                null                        // don't sort order
        );

        if (cursor.moveToFirst()) {
            Long id = cursor.getLong(cursor.getColumnIndex("id"));
            String first_name = cursor.getString(cursor.getColumnIndex("first_name"));
            String last_name = cursor.getString(cursor.getColumnIndex("last_name"));
            return new User(id, email, encodesPassword, first_name, last_name);
        }

        return null;
    }
}
