package fr.ign.vsasyan.pointsofinterest;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import fr.ign.vsasyan.pointsofinterest.dataaccess.PointOfInterestDAO;
import fr.ign.vsasyan.pointsofinterest.dataaccess.UserDAO;
import fr.ign.vsasyan.pointsofinterest.dataobject.User;

public class SignInActivity extends AppCompatActivity {

    /**
     * Déclaration des objets Java représentant les composants graphiques
     * (en attributs de la classe, ils sont donc accessibles depuis toutes les méthodes)
     */
    EditText et_email;
    EditText et_password;
    EditText et_first_name;
    EditText et_last_name;
    Button b_sign_in;

    // Attributs de classe concernant la gestion de base de données
    SQLiteDatabase db;
    UserDAO userDAO;
    PointOfInterestDAO pointOfInterestDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        // I - Instanciation les objets Java représentant les composants graphiques
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        et_first_name = findViewById(R.id.et_first_name);
        et_last_name = findViewById(R.id.et_last_name);
        b_sign_in = findViewById(R.id.b_sign_in);

        // II - Ajout des écouteurs d'événements aux composants graphiques représentés par des objets Java
        b_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SignInActivity.this.signIn();
            }
        });

        // III - Autre traitement à effectuer au début...

        // Instantiation des attributs concernants la gestion de la base de données
        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
        db = dataBaseHelper.getWritableDatabase();
        userDAO = new UserDAO(db);
        pointOfInterestDAO = new PointOfInterestDAO(db);

        // Récupération du mail passé en paramètre de l'Intent
        Intent intent = getIntent();
        if (intent != null) {
            String email = intent.getStringExtra(Constants.EXTRA_EMAIL);
            et_email.setText(email);
        }
    }

    private void signIn() {
        String email = et_email.getText().toString();
        String password = et_password.getText().toString();
        String first_name = et_first_name.getText().toString();
        String last_name = et_last_name.getText().toString();

        User user = new User(email, password, first_name, last_name);

        userDAO.create(user);

        if (user.getId() == null) {
            Toast.makeText(this, "Impossible to create the user.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "User created", Toast.LENGTH_LONG).show();
            this.finish();
        }
    }
}
