package fr.ign.vsasyan.pointsofinterest;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import fr.ign.vsasyan.pointsofinterest.dataaccess.PointOfInterestDAO;
import fr.ign.vsasyan.pointsofinterest.dataaccess.UserDAO;
import fr.ign.vsasyan.pointsofinterest.dataobject.PointOfInterest;
import fr.ign.vsasyan.pointsofinterest.dataobject.User;

public class NewPointActivity extends AppCompatActivity {

    /**
     * Déclaration des objets Java représentant les composants graphiques
     * (en attributs de la classe, ils sont donc accessibles depuis toutes les méthodes)
     */
    EditText et_title;
    EditText et_description;
    Button b_add_point;

    private LatLng currentLatLng;
    private User user;

    // Attributs de classe concernant la gestion de base de données
    SQLiteDatabase db;
    UserDAO userDAO;
    PointOfInterestDAO pointOfInterestDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_point);

        // I - Instanciation les objets Java représentant les composants graphiques
        et_title = findViewById(R.id.et_title);
        et_description = findViewById(R.id.et_description);
        b_add_point = findViewById(R.id.b_add_point);

        // II - Ajout des écouteurs d'événements aux composants graphiques représentés par des objets Java
        b_add_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewPointActivity.this.addPoint();
            }
        });

        // III - Autre traitement à effectuer au début...

        // Récupération de l'utilisateur passé en paramètre de l'Intent
        Intent intent = getIntent();
        if (intent != null) {
            user = (User)intent.getSerializableExtra(Constants.EXTRA_USER);
            Double lat = intent.getDoubleExtra(Constants.EXTRA_LAT, Double.MAX_VALUE);
            Double lng = intent.getDoubleExtra(Constants.EXTRA_LNG, Double.MAX_VALUE);
            currentLatLng = new LatLng(lat, lng);
        } else {
            Toast.makeText(this, "Error no user, no position!", Toast.LENGTH_SHORT).show();
            this.finish();
        }

        // Instantiation des attributs concernants la gestion de la base de données
        DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
        db = dataBaseHelper.getWritableDatabase();
        userDAO = new UserDAO(db);
        pointOfInterestDAO = new PointOfInterestDAO(db);
    }

    protected void addPoint() {
        String title = et_title.getText().toString();
        String description = et_description.getText().toString();

        PointOfInterest point = new PointOfInterest(title, description, currentLatLng.latitude, currentLatLng.longitude, user.getId());

        pointOfInterestDAO.create(point);

        if (point.getId() == null) {
            Toast.makeText(this, "Impossible to create the point.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Point created", Toast.LENGTH_LONG).show();
            this.finish();
        }
    }
}
