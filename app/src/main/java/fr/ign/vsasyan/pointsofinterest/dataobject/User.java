package fr.ign.vsasyan.pointsofinterest.dataobject;

public class User extends DataBaseObject {
    private String email;
    private String encodedPassword;
    private String firstName;
    private String lastName;

    // Constructor
    public User(String email, String encodedPassword, String firstName, String lastName) {
        setEmail(email);
        setEncodedPassword(encodedPassword);
        setFirstName(firstName);
        setLastName(lastName);
    }

    public User(Long id, String email, String encodedPassword, String firstName, String lastName) {
        super(id);
        setEmail(email);
        setEncodedPassword(encodedPassword);
        setFirstName(firstName);
        setLastName(lastName);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEncodedPassword() {
        return encodedPassword;
    }

    public void setEncodedPassword(String encodedPassword) {
        this.encodedPassword = encodedPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}