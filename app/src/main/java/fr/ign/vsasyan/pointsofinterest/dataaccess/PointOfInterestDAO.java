package fr.ign.vsasyan.pointsofinterest.dataaccess;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import fr.ign.vsasyan.pointsofinterest.dataobject.PointOfInterest;
import fr.ign.vsasyan.pointsofinterest.dataobject.User;

public class PointOfInterestDAO extends DAO<PointOfInterest> {

    public PointOfInterestDAO(SQLiteDatabase db) {
        super(db);
    }

    @Override
    public PointOfInterest create(PointOfInterest obj) {
        ContentValues values = new ContentValues();
        values.put("title", obj.getTitle());
        values.put("description", obj.getDescription());
        values.put("lat", obj.getLat());
        values.put("lng", obj.getLng());
        values.put("user_id", obj.getUserId());

        Long id = db.insert("point_of_interest", null, values);

        obj.setId(id);

        return obj;
    }

    @Override
    public boolean update(PointOfInterest obj) {
        ContentValues values = new ContentValues();
        values.put("title", obj.getTitle());
        values.put("description", obj.getDescription());
        values.put("lat", obj.getLat());
        values.put("lng", obj.getLng());
        values.put("user_id", obj.getUserId());

        // Which row to update, based on the title
        String selection = " id LIKE ?";
        String[] selectionArgs = { obj.getId().toString() };

        int count = db.update("point_of_interest", values, selection, selectionArgs);

        return count == 1;
    }

    @Override
    public boolean delete(PointOfInterest obj) {
        // Define 'where' part of query.
        String selection = "id = ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = { obj.getId().toString() };
        // Issue SQL statement.
        int count = db.delete("point_of_interest", selection, selectionArgs);

        return count == 1;
    }

    public ArrayList<PointOfInterest> findByUserId(Long user_id) {
        String[] columns = { "id", "title", "description", "lat", "lng", "user_id" };

        String selection = "user_id = ?";
        String[] selectionArgs = { user_id.toString() };

        Cursor cursor = db.query(
            "point_of_interest",        // The table to query
            columns,                    // The columns to return
            selection,                  // The columns for the WHERE clause
            selectionArgs,              // The values for the WHERE clause
            null,                       // don't group the rows
            null,                       // don't filter by row groups
            null                        // don't sort order
        );

        ArrayList<PointOfInterest> points = new ArrayList<>();
        while (cursor.moveToNext()) {
            Long id = cursor.getLong(cursor.getColumnIndex("id"));
            String title = cursor.getString(cursor.getColumnIndex("title"));
            String description = cursor.getString(cursor.getColumnIndex("description"));
            Double lat = cursor.getDouble(cursor.getColumnIndex("lat"));
            Double lng = cursor.getDouble(cursor.getColumnIndex("lng"));
            PointOfInterest point = new PointOfInterest(id, title, description, lat, lng, user_id);
            points.add(point);
        }

        return points;
    }
}